FROM node:latest

RUN mkdir -p /usr/app
WORKDIR /usr/app
ENV NODE_ENV production

COPY ./package.json /usr/app
COPY ./npm-shrinkwrap.json /usr/app
RUN npm i --production

# COPY ./src/ /usr/app/src
# COPY ./.babelrc /usr/app
# COPY ./app.js /usr/app

EXPOSE 3000
CMD ["npm", "start"]
